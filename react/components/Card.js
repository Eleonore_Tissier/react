import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { HeartOutlined, HeartFilled } from '@ant-design/icons'

const Card = () => {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch("https://api.deezer.com/user/2529/playlists", { mode: "no-cors", })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [])

    return (
        <div className="card">
            <div className="backColor"></div>
            <div className="box">
                <img className="gorillaz" src="./gorillaz.jpg" />
                <HeartOutlined />
                <p className="songName">New Gold</p>
                <p className="duration">3:36</p>
                <br/>
                <br/>
                <p className="artistName">Gorillaz</p>
            </div>
        </div>
    );
};

export default Card;